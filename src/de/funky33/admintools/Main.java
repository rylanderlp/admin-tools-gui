package de.funky33.admintools;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.plugin.java.JavaPlugin;
import befehle.*;

/**
 *
 * @author funky33
 */
public class Main extends JavaPlugin implements Listener {

    static HashMap<String, MinecraftCmd> commands = new HashMap<>();
    
    public static Main instance;
    
    private static void store(MinecraftCmd mc){
        commands.put(mc.getCmd(), mc);
    }
    
    public void onEnable() {
    	instance = this;
    	
    	//Startup messages
    	getServer().getPluginManager().registerEvents(this, this);
		getLogger().info("Plugin by funky33");
		getLogger().info("This is release ver. 1.0");
		
		//Config
		Config();
		
    	//Load Commands
    	loadCommands();
    }
    
    public void Config(){
		//FileConfiguration cfg = this.getConfig();
		//cfg.options().copyDefaults(true);
		//saveConfig();
    	
    	//general config
    	getConfig().addDefault("general.prefix", "&6[&7ATG&6]");
    	
    	//system outputs
    	getConfig().addDefault("system.no_permission", "&4You need the permission &e [Permission] &4 to perform this command!");
    	
    	//serverstats defaults
    	getConfig().addDefault("Message.whitelist", "&aWhitelist Status:&c");
    	getConfig().addDefault("Message.whitelisted", "&aAmount of whitelisted Players:&c");
    	getConfig().addDefault("Message.onlineplayers", "&aPlayers online:&c");
    	getConfig().addDefault("Message.serverversion", "&aServer version:&c");
    	getConfig().addDefault("Message.bannedplayers", "&aAmount of banned Players:&c");
    	getConfig().addDefault("Message.onlinemode", "&aOnlineMode:&c");
    	
		getConfig().options().copyDefaults(true);
		getConfig().options().copyHeader(true);
		saveConfig();
	}
    
    public static Main getInstance() {
		return instance;
	}

	String pre = getConfig().getString("general.prefix");
	String prefix = ChatColor.translateAlternateColorCodes('&', pre + ChatColor.RESET + " ");

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    	String no_permission = Main.getInstance().getConfig().getString("system.no_permission");
    	String no_permission_admin = no_permission.replace("[Permission]", "admintools.admin");
	    if (cmd.getName().equalsIgnoreCase("at")) {
	       	Player player = (Player)sender;
	       	if (args.length == 1 && args[0].equalsIgnoreCase("reload") && player.hasPermission("admintools.admin")) {
				reloadConfig();
				player.sendMessage(prefix + "Config reloaded");
			} else {
				if (!player.hasPermission("admintools.admin") && !player.hasPermission("admintools.open")) {
					player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_admin));
				} else if (args.length == 1) {
					player.sendMessage(prefix + ChatColor.RED + "Usage: /at reload");
				} else if (args.length == 0) {
					player.performCommand("admintools");
				} else if (args.length == 2 && args[0].equalsIgnoreCase("server")) {
					if (args[1].equalsIgnoreCase("rl")) { 
						player.performCommand("admintools server rl"); 
					}
					if (args[1].equalsIgnoreCase("stop")) { 
						player.performCommand("admintools server stop"); 
					}
				}
			}
	    } else {
	    	String befehl= cmd.getName().toString().toLowerCase(); //User input to lowercase
        	MinecraftCmd mc = commands.get(befehl); //Define Class dependig on Command
        	mc.doIt(sender, cmd, label, args, prefix); //execute Command
        
	    }
        
        return false;
    }
    
    private static void loadCommands() {
    	store(new admintools());
    	store(new cc());
    	store(new day());
    	store(new gr());
    	store(new night());
    	store(new rain());
    	store(new servercontrol());
    	store(new spec());
    	store(new sun());
    	store(new wl());
    	store(new worldcontrol());
    	store(new invsee());
    	store(new fly());
    	store(new serverstats());
    }
    
    //Event Handler
    @EventHandler
 	public void onGUIClick(InventoryClickEvent event) {
 		if (event.getView().getTitle().equalsIgnoreCase(ChatColor.GREEN + "Admin Tools") || event.getInventory().toString().equalsIgnoreCase("Admin Tools")) {
 			Player player = (Player) event.getWhoClicked();
 			event.setCancelled(true);

 			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Close")) {
 				event.setCancelled(true);
 				player.closeInventory();
 				return;
 			}
 			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Back"))	{
 				event.setCancelled(true);
 				player.performCommand("admintools");
 			}
 			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Server Control")) {
 				event.setCancelled(true);
 				player.performCommand("servercontrol");
 			}
 			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("World options")) {
 				event.setCancelled(true);
 				player.performCommand("worldcontrol");
 			}
 			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("null"))	{
 				event.setCancelled(true);
 			}
 			if (event.getClick().isLeftClick() | event.getClick().isRightClick()) {

 			}
 		}
 	}
    
    @EventHandler
	public void onControlClick(InventoryClickEvent event) {
		if (event.getView().getTitle().equalsIgnoreCase("Admin Tools / Server")) {
			Player player = (Player) event.getWhoClicked();
			event.setCancelled(true);

			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Close")) {
				player.closeInventory();
				return;
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Back"))	{
				player.performCommand("admintools");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Whitelist/on"))	{
				player.performCommand("wl on");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Whitelist/off")) {
				player.performCommand("wl off");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Whitelist/list")) {
				player.performCommand("wl list");
			}
			if (event.getCurrentItem().getType().equals(Material.REDSTONE_BLOCK)) {
				player.performCommand("servercontrol onstop");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Reload the Server")) {
				player.performCommand("admintools server rl");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Server Stats"))	{
				player.performCommand("serverstats");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("list Plugins"))	{
				player.performCommand("pl");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("null"))	{

			}
			else {

			}
		}

	}
    
    @EventHandler
	public void onStopClick(InventoryClickEvent event) {
		if (event.getView().getTitle().equalsIgnoreCase("Confirm Server Stop"))	{
			Player player = (Player) event.getWhoClicked();
			event.setCancelled(true);

			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Cancel")) {
				player.performCommand("servercontrol");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Confirm")) {
				player.performCommand("admintools server stop");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("null")) {

			}
			else {

			}
		}
	}
    
    @EventHandler
	public void onWorldClick(InventoryClickEvent event) {
		if (event.getView().getTitle().equalsIgnoreCase("Admin Tools / World")) {
			Player player = (Player) event.getWhoClicked();
			event.setCancelled(true);

			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Close")) {
				event.setCancelled(true);
				player.closeInventory();
				return;
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Back"))	{
				event.setCancelled(true);
				player.performCommand("admintools");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Day")) {
				event.setCancelled(true);
				player.performCommand("day");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Night")) {
				event.setCancelled(true);
				player.performCommand("night");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Turn weather to clear")) {
				event.setCancelled(true);
				player.performCommand("sun");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Turn Rain on"))	{
				event.setCancelled(true);
				player.performCommand("rain");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("doDaylightCycle true")) {
				event.setCancelled(true);
				player.performCommand("gr dc true");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("doDaylightCycle false")) {
				event.setCancelled(true);
				player.performCommand("gr dc false");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("doMobSpawning true")) {
				event.setCancelled(true);
				player.performCommand("gr ms true");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("doMobSpawning false")) {
				event.setCancelled(true);
				player.performCommand("gr ms false");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("KeepInventory true")) {
				event.setCancelled(true);
				player.performCommand("gr ki true");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("KeepInventory false")) {
				event.setCancelled(true);
				player.performCommand("gr ki false");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("Clear the Chat")) {
				event.setCancelled(true);
				player.performCommand("cc");
			}
			if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("null"))	{
				event.setCancelled(true);
			}
			else {

			}
		}

	}
}
