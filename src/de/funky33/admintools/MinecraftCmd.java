/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.funky33.admintools;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 *
 * @author funky33
 */
public abstract class MinecraftCmd {
    private String cmd;
    
    public MinecraftCmd(String cmd) {
        this.cmd = cmd;
    }

	public String getCmd() {
        return cmd;
    }
    
    public abstract void doIt(CommandSender sender, Command cmd, String label, String[] args, String prefix);
    
}
