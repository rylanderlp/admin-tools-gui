package befehle;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.funky33.admintools.Main;
import de.funky33.admintools.MinecraftCmd;

public class servercontrol extends MinecraftCmd implements Listener {
	
	public ArrayList<String> LoreStats =new ArrayList<String>();
	public ArrayList<String> Lorepl = new ArrayList<String>();
	public ArrayList<String> Lorewlist = new ArrayList<String>();
	
	public servercontrol() {
		super("servercontrol");
	}
	
	public void doIt(CommandSender sender, Command cmd, String label, String[] args, String prefix) {
		Player player = (Player)sender;
		String no_permission = Main.getInstance().getConfig().getString("system.no_permission");
    	String no_permission_open = no_permission.replace("[Permission]", "admintools.open");
    	
		if (cmd.getName().equalsIgnoreCase("servercontrol")) {
			if (player.hasPermission("admintools.open") | player.hasPermission("admintools.admin"))	{
				if (args.length < 1) {
					openControl(player.getPlayer());
				} else if (args.length == 1 && args[0].equalsIgnoreCase("onstop")) {
					openStop(player.getPlayer());
				}
				
			} else {
				player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_open));
			}
		}
	}
	
	public void openControl(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9*3, "Admin Tools / Server");

		ItemStack wlon = new ItemStack(Material.GREEN_STAINED_GLASS_PANE, 1);
		ItemMeta wlonmeta = wlon.getItemMeta();
		ItemStack wloff = new ItemStack(Material.RED_STAINED_GLASS_PANE, 1);
		ItemMeta wloffmeta = wloff.getItemMeta();
		ItemStack wllist = new ItemStack(Material.WHITE_STAINED_GLASS_PANE, 1);
		ItemMeta wllistmeta = wllist.getItemMeta();
		ItemStack stop = new ItemStack(Material.REDSTONE_BLOCK);
		ItemMeta stopmeta = stop.getItemMeta();
		ItemStack reload = new ItemStack(Material.TRIPWIRE_HOOK);
		ItemMeta reloadmeta = reload.getItemMeta();
		ItemStack close = new ItemStack(Material.BARRIER);
		ItemMeta closemeta = close.getItemMeta();
		ItemStack back = new ItemStack(Material.ARROW);
		ItemMeta backmeta = back.getItemMeta();
		ItemStack stats = new ItemStack(Material.PAPER);
		ItemMeta statsmeta = stats.getItemMeta();
		ItemStack filler = new ItemStack(Material.GRAY_STAINED_GLASS_PANE, 1);
		ItemMeta fillermeta = filler.getItemMeta();
		ItemStack pl = new ItemStack(Material.PAPER);
		ItemMeta plmeta = pl.getItemMeta();

		fillermeta.setDisplayName("null");
		filler.setItemMeta(fillermeta);

		plmeta.setDisplayName("list Plugins");
		Lorepl.clear();
		Lorepl.add(0, ChatColor.DARK_RED + "Requires OP");
		plmeta.setLore(Lorepl);
		pl.setItemMeta(plmeta);

		statsmeta.setDisplayName("Server Stats");
		LoreStats.clear();
		LoreStats.add(0, "Requires ServerStats Plugin");
		statsmeta.setLore(LoreStats);
		stats.setItemMeta(statsmeta);

		backmeta.setDisplayName("Back");
		back.setItemMeta(backmeta);

		stopmeta.setDisplayName("Stop the Server");
		stop.setItemMeta(stopmeta);

		reloadmeta.setDisplayName("Reload the Server");
		reload.setItemMeta(reloadmeta);

		closemeta.setDisplayName("Close");
		close.setItemMeta(closemeta);

		wlonmeta.setDisplayName("Whitelist/on");
		wlon.setItemMeta(wlonmeta);

		wloffmeta.setDisplayName("Whitelist/off");
		wloff.setItemMeta(wloffmeta);

		wllistmeta.setDisplayName("Whitelist/list");
		Lorewlist.clear();
		Lorewlist.add(0, ChatColor.DARK_RED + "Requires OP");
		wllistmeta.setLore(Lorewlist);
		wllist.setItemMeta(wllistmeta);

		inv.setItem(0, filler);
		inv.setItem(1, wlon);
		inv.setItem(2, filler);
		inv.setItem(3, filler);
		inv.setItem(4, filler);
		inv.setItem(5, filler);
		inv.setItem(6, stop);
		inv.setItem(7, filler);
		inv.setItem(8, filler);
		inv.setItem(9, filler);
		inv.setItem(10, wloff);
		inv.setItem(11, filler);
		inv.setItem(12, filler);
		inv.setItem(13, stats);
		inv.setItem(14, filler);
		inv.setItem(15, reload);
		inv.setItem(16, filler);
		inv.setItem(17, filler);
		inv.setItem(18, filler);
		inv.setItem(19, filler);
		inv.setItem(20, filler);
		inv.setItem(21, filler);
		inv.setItem(22, filler);
		inv.setItem(23, filler);
		inv.setItem(24, pl);
		inv.setItem(25, back);
		inv.setItem(26, close);

		player.openInventory(inv);
	}
	

	// Stop
	public void openStop(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, "Confirm Server Stop");

		ItemStack stop = new ItemStack(Material.GREEN_STAINED_GLASS, 1);
		ItemMeta stopmeta = stop.getItemMeta();
		ItemStack close = new ItemStack(Material.BARRIER);
		ItemMeta closemeta = close.getItemMeta();
		ItemStack filler = new ItemStack(Material.GRAY_STAINED_GLASS_PANE, 1);
		ItemMeta fillermeta = filler.getItemMeta();

		fillermeta.setDisplayName("null");
		filler.setItemMeta(fillermeta);

		closemeta.setDisplayName("Cancel");
		close.setItemMeta(closemeta);

		stopmeta.setDisplayName("Confirm");
		stop.setItemMeta(stopmeta);

		inv.setItem(0, filler);
		inv.setItem(1, filler);
		inv.setItem(2, stop);
		inv.setItem(3, filler);
		inv.setItem(4, filler);
		inv.setItem(5, filler);
		inv.setItem(6, close);
		inv.setItem(7, filler);
		inv.setItem(8, filler);

		player.openInventory(inv);
	}
	
}
