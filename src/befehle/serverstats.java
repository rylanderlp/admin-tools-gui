package befehle;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.funky33.admintools.Main;
import de.funky33.admintools.MinecraftCmd;
import net.md_5.bungee.api.ChatColor;

public class serverstats extends MinecraftCmd {
	public serverstats() {
		super("serverstats");
	}
	
	public void doIt(CommandSender sender, Command cmd, String label, String[] args, String prefix) {
		Player player = (Player)sender;
		Server server = Bukkit.getServer();
		String no_permission = Main.getInstance().getConfig().getString("system.no_permission");
    	String no_permission_stats = no_permission.replace("[Permission]", "admintools.stats");
		
		if (cmd.getName().equalsIgnoreCase("serverstats")) {
			if (player.hasPermission("admintools.stats") | player.hasPermission("admintools.admin")) {
				if (args.length == 0) {
					String whitelist = Main.getInstance().getConfig().getString("Message.whitelist");
					String whitelisted = Main.getInstance().getConfig().getString("Message.whitelisted");
					String onlineplayers = Main.getInstance().getConfig().getString("Message.onlineplayers");
					String serverversion = Main.getInstance().getConfig().getString("Message.serverversion");
					String bannedplayers = Main.getInstance().getConfig().getString("Message.bannedplayers");
					String onlinemode = Main.getInstance().getConfig().getString("Message.onlinemode");
					
					player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', "&6Server Status Monitor:"));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', serverversion + " " + server.getVersion()));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', onlinemode + " " + server.getOnlineMode()));
					player.sendMessage("Motd: " + server.getMotd());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', bannedplayers + " " + server.getBannedPlayers().size()));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', whitelist + " " + server.hasWhitelist()));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', whitelisted + " " + server.getWhitelistedPlayers().size()));
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', onlineplayers + " " + server.getOnlinePlayers().size() + "/" + server.getMaxPlayers()));
				} else {
					player.sendMessage(prefix + ChatColor.RED + "Too many arguments Usage: /serverstats");
				}
			} else {
				player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_stats));
			}
		}
	}
}
