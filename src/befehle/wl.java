package befehle;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.funky33.admintools.Main;
import de.funky33.admintools.MinecraftCmd;

/**
 *
 * @author funky33
 */
public class wl extends MinecraftCmd {
    
    public wl() {
        super("wl"); //Command in Chat
    }

    @Override
    public void doIt(CommandSender sender, Command cmd, String label, String[] args, String prefix) { //Waht will be executed
    	Player player = (Player)sender;
    	String no_permission = Main.getInstance().getConfig().getString("system.no_permission");
    	String no_permission_wl = no_permission.replace("[Permission]", "admintools.wl");
    	
    	if (cmd.getName().equalsIgnoreCase("wl")) {
			if (player.hasPermission("admintools.wl") | player.hasPermission("admintools.admin")) {
				if (args.length == 1 | args.length == 2) {
					if (args[0].equalsIgnoreCase("on")) {
						Bukkit.setWhitelist(true);
						player.sendMessage(prefix + "Whitelist turned on");
					}
					if (args[0].equalsIgnoreCase("off")) {
						Bukkit.setWhitelist(false);
						player.sendMessage(prefix + "Whitelist turned off");
					}
					/*  Not working at the moment

					if (args[0].equalsIgnoreCase("list")) {
						player.performCommand("whitelist list");
					}
					if (args[0].equalsIgnoreCase("add")) {
						String target = args[1];
						@SuppressWarnings("deprecation")
						OfflinePlayer p = Bukkit.getOfflinePlayer(target);
						Bukkit.getWhitelistedPlayers().add(p);
						player.sendMessage(prefix + "Added " + target + " to Whitelist");
					}
					if (args[0].equalsIgnoreCase("rem") | args[0].equalsIgnoreCase("remove")) {
						String target = args[1];
						@SuppressWarnings("deprecation")
						OfflinePlayer p = Bukkit.getOfflinePlayer(target);
						Bukkit.getWhitelistedPlayers().remove(p);
						player.sendMessage(prefix + "Removed " + target + " from Whitelist");
					}
					*/
				} else if (args.length > 2) {
					sender.sendMessage(prefix + ChatColor.RED + "To many arguments Usage: /wl <on/off> ");
				} else if (args.length < 1) {
					sender.sendMessage(prefix + ChatColor.RED + "To few arguments Usage: /wl <on/off> ");
				}
			} else {
				player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', no_permission_wl));
			}
		}
    }
}
